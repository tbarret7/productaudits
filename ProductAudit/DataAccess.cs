﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;


namespace ProductAudit
{
    class DataAccess
    {
        string ConnectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_PRODUCT_AUDITS; Integrated Security = SSPI";
        string Oradb = "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                    + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                    + "User Id=glovia_prod;Password=manager;";

        OracleConnection conn;
        SqlConnection con;

        public void OpenConnection()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }
        public void OracleConnection()
        {
            conn = new OracleConnection(Oradb);
            conn.Open();
        }

        public void CloseConnection()
        {
            con.Close();
        }
        public void OracleCloseConnection()
        {
            conn.Close();
        }

        public void ExecuteQueries(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);            
            cmd.ExecuteNonQuery();
            
        }
        public void OracleExecuteQueries(string Query_)
        {
            OracleCommand cmd = new OracleCommand(Query_, conn);
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public SqlDataReader DataReader(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public OracleDataReader ODataReader(string Query_)
        {
            OracleCommand cmd = new OracleCommand(Query_, conn);
            OracleDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public SqlDataAdapter DataAdapter(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            return adapter;

        }

        public object ShowDataInGridView(string Query_)
        {
            SqlDataAdapter dr = new SqlDataAdapter(Query_, ConnectionString);
            DataSet ds = new DataSet();
            dr.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;
        }
    }
}
