﻿namespace ProductAudit
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.tblQualityAuditsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.GEI_PRODUCT_AUDITSDataSet1 = new ProductAudit.GEI_PRODUCT_AUDITSDataSet1();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tblQualityAuditsTableAdapter1 = new ProductAudit.GEI_PRODUCT_AUDITSDataSet1TableAdapters.tblQualityAuditsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.tblQualityAuditsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_PRODUCT_AUDITSDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // tblQualityAuditsBindingSource1
            // 
            this.tblQualityAuditsBindingSource1.DataMember = "tblQualityAudits";
            this.tblQualityAuditsBindingSource1.DataSource = this.GEI_PRODUCT_AUDITSDataSet1;
            // 
            // GEI_PRODUCT_AUDITSDataSet1
            // 
            this.GEI_PRODUCT_AUDITSDataSet1.DataSetName = "GEI_PRODUCT_AUDITSDataSet1";
            this.GEI_PRODUCT_AUDITSDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(396, 246);
            this.reportViewer1.TabIndex = 0;
            // 
            // reportViewer2
            // 
            this.reportViewer2.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.tblQualityAuditsBindingSource1;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "ProductAudit.Report1.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(0, 0);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ServerReport.BearerToken = null;
            this.reportViewer2.Size = new System.Drawing.Size(800, 450);
            this.reportViewer2.TabIndex = 0;
            this.reportViewer2.Load += new System.EventHandler(this.ReportViewer2_Load);
            // 
            // tblQualityAuditsTableAdapter1
            // 
            this.tblQualityAuditsTableAdapter1.ClearBeforeFill = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.Text = "Report viewer";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblQualityAuditsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_PRODUCT_AUDITSDataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private GEI_PRODUCT_AUDITSDataSet1 GEI_PRODUCT_AUDITSDataSet1;
        private System.Windows.Forms.BindingSource tblQualityAuditsBindingSource1;
        private GEI_PRODUCT_AUDITSDataSet1TableAdapters.tblQualityAuditsTableAdapter tblQualityAuditsTableAdapter1;
    }
}