﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using Microsoft.SqlServer.Types;
using System.Data.SqlClient;

namespace ProductAudit
{
    public partial class Form5 : Form
    {
        string die = "";
        string pn = "";
        string geipn = "";

        public Form5(string q, string s, string ln, string Die, string custPN)
        {
            InitializeComponent();
            textBox2.Text = q;
            textBox5.Text = s;
            geipn = s;
            textBox6.Text = ln;
            die = Die;
            pn = custPN;
        }
        Bitmap memoryImage;
        

        private void CaptureScreen()
        {
            printDocument1.DefaultPageSettings.Landscape = true;
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            
            textBox3.Text = Environment.UserName.ToString();
            textBox4.Text = DateTime.Today.ToShortDateString();
            
            DataAccess da = new DataAccess();
            da.OpenConnection();
            string qry = "SELECT ImageAttachment from tblAuditRecipe WHERE PartNumber = '" + geipn + "'";
            da.ExecuteQueries(qry);
            SqlDataReader reader = da.DataReader(qry);

            try
            {


                while (reader.Read())
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["ImageAttachment"]);
                    pictureBox2.Image = Image.FromStream(ms);
                    textBox1.Text = "GEI PN: " + geipn + "   CUST PN: " + pn;
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            System.Drawing.Printing.PrintDocument doc = new System.Drawing.Printing.PrintDocument();
            doc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocument1_PrintPage);
            //printDialog1.PrinterSettings = default;
            PaperSize ps = new PaperSize("Custom", 400, 600);
            doc.DefaultPageSettings.Landscape = true;
            doc.DefaultPageSettings.PaperSize = ps;
            



            printDialog1.Document = doc;
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                doc.Print();
            }
            else
            {
                MessageBox.Show("Print Cancelled..");
            }
            
            
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            
            Bitmap bmp = new Bitmap(grd.Width, grd.Height, grd.CreateGraphics());
            grd.DrawToBitmap(bmp, new Rectangle(0, 0, grd.Width, grd.Height));
            RectangleF bounds = e.MarginBounds;            
            e.Graphics.DrawImage(bmp, e.PageBounds);

        }               

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBox1.SelectedText.ToString();


            //if (comboBox1.SelectedIndex == 0)
            //{
            //    textBox1.Text = selected + " ODL CASSETTE HOUSING 26773-901/902/903 GEI CCN 4290 100 % CERTIFIED FOR: PUNCHED FEATURE PRESENCE, DOUBLE PUNCHED, &MISLOCATED PUNCH FEATURES ";
            //}
            //if (comboBox1.SelectedIndex == 1)
            //{
            //    textBox1.Text = selected + " ODL CASSETTE HOUSING SLIDER 25869 - 901 AUIDITTED FOR PRODUCT CONFORMANCE: QA APPROVED";
            //}
            //if (comboBox1.SelectedIndex == 2)
            //{
            //    textBox1.Text = selected + " ODL DECORATIVE CASING 26774 - 901 / 902 / 903 100 % CERTIFIED FOR: PUNCHED FEATURE PRESENCE";
            //}
            //if (comboBox1.SelectedIndex == 3)
            //{
            //    textBox1.Text = selected + " ODL, INC. HURRICANE FRAME ODL ITEM #S: 23081, 24803, 24804-XXX GEI ITEM #S: 83173, 83174, 83175-XXX AQL AUDITED .65 % FOR COMPLETE CUT TO SIZE FEATURE PER GEI CAR 365; ODL SCAR 18 - 030";
            //}
            //if (comboBox1.SelectedIndex == 4)
            //{
            //    textBox1.Text = selected + " ODL, INC. SEVERE WEATHER ODL ITEM #S: 27565, 27566, 27783, 27790-XXX GEI ITEM #S: 83339, 83340, 83341, 83342-XXX 100 % CERTIFIED FOR COMPLETE CUT TO SIZE FEATURE &CORRECT DIE PROFILE PER GEI CAR 365; ODL SCAR 18 - 030 PER GEI CAR 369; ODL SCAR 18 - 060 PER GEi CCN 5166 / ODL SUPPLIER DMR 4.4.19";
            //}
            //if (comboBox1.SelectedIndex == 5)
            //{
            //    textBox1.Text = selected + " REFERENCE ODL SCAR # 174-004 GEI PART # 83034-XX  / CAR 348 MATERIAL HAS BEEN 100 % CERTIFIED THAT CHIPS/ PIG TAILS IN EXTRUDED HOLLOW HAVE BEEN REMOVED.";
            //}

        }
    }
}
