﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;




namespace ProductAudit
{
    public partial class Form1 : Form
    {
        public bool Landscape { get; set; }

        public Form1()
        {
            InitializeComponent();
            
        }

        public static bool IsInGroup(string ingroup)
        {
            string username = Environment.UserName;

            PrincipalContext domainctx = new PrincipalContext(ContextType.Domain, "Gei.Local");

            UserPrincipal userPrincipal =
                              UserPrincipal.FindByIdentity(domainctx, IdentityType.SamAccountName, username);

            bool isMember = userPrincipal.IsMemberOf(domainctx, IdentityType.Name, ingroup);

            return isMember;
        }




        private void button1_Click_1(object sender, EventArgs e)
        {
            string d1 = "";
            string d2 = "";
            string d3 = "";
            string d4 = "";
            string d5 = "";
            string d6 = "";
            string d7 = "";
            string d8 = "";
            string d9 = "";
            string d10 = "";
            string c1 = "";
            string c2 = "";
            string c3 = "";
            string c4 = "";
            string c5 = "";
            string c6 = "";
            string c7 = "";
            string c8 = "";
            string c9 = "";
            string c10 = "";
            string dd1 = "";
            string dd2 = "";
            string dd3 = "";
            string dd4 = "";
            string dd5 = "";
            string dd6 = "";
            string dd7 = "";
            string dd8 = "";
            string dd9 = "";
            string dd10 = "";
            string auditType = "";
            string partNum = "";
            string qtyOrd = "";
            string rev = "";
            string inspectlvl = "";
            string cust = "";
            string sampleSize = "";
            string custPartNum = "";
            string custRev = "";
            string OperSeq = "";
            string dieNum = "";
            string lot_Size = "";
            string shiftNum = "";
            string adc = "";
            string dispo = "";
            string closeddate = "";
            string DMRNum = "";
            string operNo = "";
            string aName = "";
            string qtys = "";
            string qtys2 = "";
            string qtys3 = "";
            string qtys4 = "";
            string qtys5 = "";
            string qtys6 = "";
            string qtys7 = "";
            string qtys8 = "";
            string qtys9 = "";
            string qtys10 = "";
            string qtyd = "";
            string qtyd2 = "";
            string qtyd3 = "";
            string qtyd4 = "";
            string qtyd5 = "";
            string qtyd6 = "";
            string qtyd7 = "";
            string qtyd8 = "";
            string qtyd9 = "";
            string qtyd10 = "";
            string dateTime = "";




            auditType = comboBox2.Text.ToString();
            partNum = textBox2.Text;
            qtyOrd = textBox1.Text;
            rev = textBox4.Text;
            inspectlvl = comboBox3.Text.ToString();
            cust = textBox5.Text;
            sampleSize = comboBox4.Text.ToString();
            custPartNum = textBox6.Text;
            custRev = textBox7.Text;
            OperSeq = textBox10.Text;
            dieNum = textBox3.Text;
            lot_Size = textBox11.Text;
            shiftNum = textBox12.Text;

            d1 = comboBox5.Text.ToString();
            d2 = comboBox6.Text.ToString();
            d3 = comboBox7.Text.ToString();
            d4 = comboBox8.Text.ToString();
            d5 = comboBox9.Text.ToString();
            d6 = comboBox10.Text.ToString();
            d7 = comboBox11.Text.ToString();
            d8 = comboBox12.Text.ToString();
            d9 = comboBox13.Text.ToString();
            d10 = comboBox14.Text.ToString();

            // MessageBox.Show(d1);

            c1 = comboBox15.Text.ToString();
            c2 = comboBox16.Text.ToString();
            c3 = comboBox17.Text.ToString();
            c4 = comboBox18.Text.ToString();
            c5 = comboBox19.Text.ToString();
            c6 = comboBox20.Text.ToString();
            c7 = comboBox21.Text.ToString();
            c8 = comboBox22.Text.ToString();
            c9 = comboBox23.Text.ToString();
            c10 = comboBox24.Text.ToString();

            dd1 = comboBox25.Text.ToString();
            dd2 = comboBox26.Text.ToString();
            dd3 = comboBox27.Text.ToString();
            dd4 = comboBox28.Text.ToString();
            dd5 = comboBox29.Text.ToString();
            dd6 = comboBox30.Text.ToString();
            dd7 = comboBox31.Text.ToString();
            dd8 = comboBox32.Text.ToString();
            dd9 = comboBox33.Text.ToString();
            dd10 = comboBox34.Text.ToString();

            qtys = textBox13.Text;
            qtys2 = textBox14.Text;
            qtys3 = textBox15.Text;
            qtys4 = textBox16.Text;
            qtys5 = textBox17.Text;
            qtys6 = textBox18.Text;
            qtys7 = textBox19.Text;
            qtys8 = textBox20.Text;
            qtys9 = textBox21.Text;
            qtys10 = textBox22.Text;

            qtyd = textBox23.Text;
            qtyd2 = textBox24.Text;
            qtyd3= textBox25.Text;
            qtyd4 = textBox26.Text;
            qtyd5 = textBox27.Text;
            qtyd6 = textBox28.Text;
            qtyd7 = textBox29.Text;
            qtyd8 = textBox30.Text;
            qtyd9 = textBox31.Text;
            qtyd10 = textBox32.Text;

            adc = textBox33.Text;
            dispo = comboBox1.Text.ToString();
            closeddate = textBox34.Text;
            DMRNum = textBox35.Text;
            operNo = textBox37.Text;
            aName = textBox36.Text;
            string wonum = textBox38.Text;
            string linenum = textBox39.Text;
            string oper = textBox40.Text;

            dateTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm");





            //SQL connection to update table -------------------------------------------------------------------------------

            DataAccess da = new DataAccess();
            da.OpenConnection();

            string insertqry = "Insert into tblQualityAudits (AuditType, Date, PartNumber, " +
                "Rev, InspectionLevel, Customer, ShopOrder, SampleSize, CustomerPartNumber, " +
                "CustomerRev, OperationSeq, DieNumber, Lot_Size, ShiftNum, Description, Description2, " +
                "Description3, Description4, Description5, Description6, Description7, Description8, " +
                "Description9, Description10, CheckMethod1, CheckMethod2, CheckMethod3, CheckMethod4, " +
                "CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10, " +
                "DefectDescription1, DefectDescription2, DefectDescription3, DefectDescription4, DefectDescription5, " +
                "DefectDescription6, DefectDescription7, DefectDescription8, DefectDescription9, DefectDescription10, " +
                "AuditorDefectComments, Disposition, ClosedDate, DMRNum, OperNo, AuditorName, QtySampled, QtyDisc, " +
                "QtySampled2, QtyDisc2, QtySampled3, QtyDisc3, QtySampled4, QtyDisc4, QtySampled5, QtyDisc5, QtySampled6, QtyDisc6, " +
                "QtySampled7, QtyDisc7, QtySampled8, QtyDisc8, QtySampled9, QtyDisc9, QtySampled10, QtyDisc10, WONum, Line, Oper) Select '" + auditType + "' as AuditType_Ins, '"
                 + dateTime + "' as Date_Ins, '" + partNum + "' as PartNumber_Ins, '" + rev + "' as Rev_Ins, '" + inspectlvl + "' as InspectionLevel_Ins, '" + cust + "' as Customer_Ins, '" + qtyOrd + "' as ShopOrder_Ins, '"
                 + sampleSize + "' as SampleSize_Ins, '" + custPartNum + "' as CustomerPartNumber_Ins, '" + custRev + "' as CustomerRev_Ins, '" + OperSeq + "' as OperationSeq_Ins, '"
                 + dieNum + "' as DieNumber_Ins, '" + lot_Size + "' as Lot_Size_Ins, '" + shiftNum + "' as ShiftNum_Ins, '" + d1 + "' as Description_Ins, '"
                 + d2 + "'as Description2_Ins, '" + d3 + "' as Description3_Ins, '" + d4 + "' as Descritpion4_Ins, '" + d5 + "' as Description5_Ins, '" + d6 + "' as Description6_Ins, '"
                 + d7 + "' as Description7_Ins, '" + d8 + "' as Description8_Ins, '" + d9 + "' as Description9_Ins, '" + d10 + "' as Description10_Ins, '"
                 + c1 + "' as CheckMethod1_Ins, '" + c2 + "' as CheckMethod2_Ins, '" + c3 + "' as CheckMethod3_Ins, '" + c4 + "' as CheckMethod4_Ins, '" + c5 + "' as CheckMethod5_Ins, '"
                 + c6 + "' as CheckMethod6_Ins, '" + c7 + "' as CheckMethod7_Ins, '" + c8 + "' as CheckMethod8_Ins, '" + c9 + "' as CheckMethod9_Ins, '" + c10 + "' as CheckMethod10_Ins, '"
                 + dd1 + "' as DefectDescription1_Ins, '" + dd2 + "' as DefectDescription2_Ins, '" + dd3 + "' as DefectDescription3_Ins, '" + dd4 + "' as DefectDescription4_Ins, '"
                 + dd5 + "' as DefectDescription5_Ins, '" + dd6 + "' as DefectDescription6_Ins, '" + dd7 + "' as DefectDescription7_Ins, '" + dd8 + "' as DefectDescription8_Ins, '"
                 + dd9 + "' as DefectDescription9_Ins, '" + dd10 + "' as DefectDescription10_Ins, '" + adc + "' as AuditorDefectComments_Ins, '"
                 + dispo + "' as Disposition_Ins, '" + closeddate + "' as ClosedDate_Ins, '" + DMRNum + "' as DMRNum_Ins, '" + operNo + "' as OperNo_Ins, '"
                 + aName + "' as AuditorName_Ins, '" + qtys + "' as QtySampled_Ins, '" + qtyd + "' as QtyDisc_Ins, '" + qtys2 + "' as QtySampled2_Ins, '"
                 + qtyd2 + "' as QtyDisc2_Ins, '" + qtys3 + "' as QtySampled3_Ins, '" + qtyd3 + "' as QtyDisc3_Ins, '" + qtys4 + "' as QtySampled4_Ins, '"
                 + qtyd4 + "' as QtyDisc4_Ins, '" + qtys5 + "' as QtySampled5_Ins, '" + qtyd5 + "' as QtyDisc5_Ins, '" + qtys6 + "' as QtySampled6_Ins, '"
                 + qtyd6 + "' as QtyDisc6_Ins, '" + qtys7 + "' as QtySampled7_Ins, '" + qtyd7 + "' as QtyDisc7_Ins, '" + qtys8 + "' as QtySampled8_Ins, '"
                 + qtyd8 + "' as QtyDisc8_Ins, '" + qtys9 + "' as QtySampled9_Ins, '" + qtyd9 + "' as QtyDisc9_Ins, '" + qtys10 + "' as QtySampled10_Ins, '"
                 + qtyd10 + "' as QtyDisc10_Ins, '" + wonum + "' as WONum_Ins, '" + linenum + "' as Line_Ins, '" + oper + "' as Oper_Ins";

            da.ExecuteQueries(insertqry);
            da.CloseConnection();

            

            MessageBox.Show("You have successfully input a new produt audit.");

            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();            
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
            textBox13.Clear();
            textBox14.Clear();
            textBox15.Clear();
            textBox16.Clear();
            textBox17.Clear();
            textBox18.Clear();
            textBox19.Clear();
            textBox20.Clear();
            textBox21.Clear();
            textBox22.Clear();
            textBox23.Clear();
            textBox24.Clear();
            textBox25.Clear();
            textBox26.Clear();
            textBox27.Clear();
            textBox28.Clear();
            textBox29.Clear();
            textBox30.Clear();
            textBox31.Clear();
            textBox32.Clear();
            textBox33.Clear();
            textBox34.Clear();
            textBox35.Clear();
            textBox36.Clear();
            textBox37.Clear();
            textBoxx1.Clear();
            textBoxx2.Clear();
            textBoxx3.Clear();
            comboBox1.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            comboBox4.Text = "";
            comboBox5.Text = "";
            comboBox6.Text = "";
            comboBox7.Text = "";
            comboBox8.Text = "";
            comboBox9.Text = "";
            comboBox10.Text = "";
            comboBox11.Text = "";
            comboBox12.Text = "";
            comboBox13.Text = "";
            comboBox14.Text = "";
            comboBox15.Text = "";
            comboBox16.Text = "";
            comboBox17.Text = "";
            comboBox18.Text = "";
            comboBox19.Text = "";
            comboBox20.Text = "";
            comboBox21.Text = "";
            comboBox22.Text = "";
            comboBox23.Text = "";
            comboBox24.Text = "";
            comboBox25.Text = "";
            comboBox26.Text = "";
            comboBox27.Text = "";
            comboBox28.Text = "";
            comboBox29.Text = "";
            comboBox30.Text = "";
            comboBox31.Text = "";
            comboBox32.Text = "";
            comboBox33.Text = "";
            comboBox34.Text = "";
            textBox33.Text = "";
            textBox34.Text = "";
            textBox35.Text = "";
            textBox36.Text = "";
            textBox37.Text = "";

            //END SQL connection to update table -----------------------------------------------------------------------------
        }



        private void Button3_Click_1(object sender, EventArgs e)
        {
            try
            {

                //Update form2 with queuried data based on txtbox1,2,3
                string woNum = textBoxx1.Text;
                string woLine = textBoxx2.Text;
                string woOper = textBoxx3.Text;
                string geiItem = "";
                string geiRev = "";
                textBox38.Text = woNum;
                textBox39.Text = woLine;
                textBox40.Text = woOper;

               //Init Oracle connection -------------------------------------------------------------------------------------

                DataAccess da = new DataAccess();
                da.OracleConnection();

                string qry = "Select "
                    + "WO.CCN As CCN, "
                    + "WO.MAS_LOC As ML, "
                    + "LTrim(WO.WO_NUM) As NumTrim, "
                    + "WO.WO_LINE As WOL, "
                    + "LTrim(WO_RTG.OPERATION) As OperTrim, "
                    + "WO.ORD_QTY As ORDQTY, "
                    + "WO.ITEM, "
                    + "WO.REVISION, "
                    + "BOM_HDR.BOM_DESCRIPTION, "
                    + "ITEM_CUS.CUSTOMER, "
                    + "ITEM_CUS.CUS_ITEM, "
                    + "ITEM_CUS.CUS_REV, "
                    + "BOM_HDR.BCR_TYPE, "
                    + "ITEM_CCN.USER1, "
                    + "ITEM_CCN.USER2"
                      + " From "
                    + "WO Inner Join "
                     + "WO_RTG On WO_RTG.CCN = WO.CCN "
                    + "And WO_RTG.MAS_LOC = WO.MAS_LOC "
                    + "And WO_RTG.WO_NUM = WO.WO_NUM "
                    + "And WO_RTG.WO_LINE = WO.WO_LINE Inner Join "
                    + "ITEM_CCN On ITEM_CCN.ITEM = WO.ITEM "
                    + "And ITEM_CCN.REVISION = WO.REVISION Inner Join "
                    + "BOM_HDR On BOM_HDR.ITEM = ITEM_CCN.ITEM "
                    + "And BOM_HDR.REVISION = ITEM_CCN.REVISION Left Join "
                    + "ITEM_CUS On ITEM_CUS.ITEM = BOM_HDR.ITEM "
                    + "And ITEM_CUS.REVISION = BOM_HDR.REVISION"
                      + " Where "
                    + "LTrim(WO.WO_NUM) = '" + woNum + "' And "
                    + "WO.WO_LINE = '" + woLine + "' And "
                    + "LTrim(WO_RTG.OPERATION) = '" + woOper + "' And "
                    + "BOM_HDR.BCR_TYPE = 'CUR' "
                    + "Group By "
                    + "WO.CCN, "
                    + "WO.MAS_LOC, "
                    + "LTrim(WO.WO_NUM), "
                    + "WO.WO_LINE, "
                    + "LTrim(WO_RTG.OPERATION), "
                    + "WO.ORD_QTY, "
                    + "WO.ITEM, "
                    + "WO.REVISION, "
                    + "BOM_HDR.BOM_DESCRIPTION, "
                    + "ITEM_CUS.CUSTOMER, "
                    + "ITEM_CUS.CUS_ITEM, "
                    + "ITEM_CUS.CUS_REV, "
                    + "BOM_HDR.BCR_TYPE, "
                    + "ITEM_CCN.USER1, "
                    + "ITEM_CCN.USER2";

                da.OracleExecuteQueries(qry);               

                OracleDataReader dr = da.ODataReader(qry);
                dr.Read();

                while (dr.Read())
                {
                    textBox1.Text = dr["ORDQTY"].ToString();
                    textBox2.Text = dr["ITEM"].ToString();
                    textBox3.Text = dr["USER2"].ToString();
                    textBox4.Text = dr["REVISION"].ToString();
                    textBox6.Text = dr["CUS_ITEM"].ToString();
                    textBox7.Text = dr["CUS_REV"].ToString();
                    textBox8.Text = dr["NumTrim"].ToString();
                    textBox9.Text = dr["WOL"].ToString();
                    textBox10.Text = dr["OperTrim"].ToString();

                    geiItem = dr["ITEM"].ToString();
                    geiRev = dr["REVISION"].ToString();
                    
                }

                dr.Close();
                da.OracleCloseConnection();

                //End Oracle Connection---------------------------------------------------------------

                //Init SQL Connection-----------------------------------------------------------------
                                                    
                da.OpenConnection();                

                //Recipe Qry
                string recipeqry = "Select PartNumber, Rev, Description, Description2, Description3, Description4, Description5, Description6, "
                    + "Description7, Description8, Description9, Description10, CheckMethod, CheckMethod2, CheckMethod3, "
                    + "CheckMethod4, CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10 FROM tblAuditRecipe WHERE PartNumber = '"
                    + geiItem + "' and Rev = '" + geiRev + "'";

                //DefectDescription Qry
                string defectdesc = "Select DefectDescription from tblDefectDescriptions";

                //Description Qry
                string desc = "Select Description from tblDescriptions";

                //CheckMethod Qry
                string checkmethod = "Select CheckMethod from tblCheckMethods";

                da.ExecuteQueries(recipeqry);
                da.ExecuteQueries(defectdesc);
                da.ExecuteQueries(desc);
                da.ExecuteQueries(checkmethod);
                
                SqlDataReader myreader = da.DataReader(recipeqry);

                while (myreader.Read())
                {
                    try
                    {

                        comboBox5.Text = (myreader["Description"].ToString());
                        comboBox6.Text = (myreader["Description2"].ToString());
                        comboBox7.Text = (myreader["Description3"].ToString());
                        comboBox8.Text = (myreader["Description4"].ToString());
                        comboBox9.Text = (myreader["Description5"].ToString());
                        comboBox10.Text = (myreader["Description6"].ToString());
                        comboBox11.Text = (myreader["Description7"].ToString());
                        comboBox12.Text = (myreader["Description8"].ToString());
                        comboBox13.Text = (myreader["Description9"].ToString());
                        comboBox14.Text = (myreader["Description10"].ToString());
                        comboBox15.Text = (myreader["CheckMethod"].ToString());
                        comboBox16.Text = (myreader["CheckMethod2"].ToString());
                        comboBox17.Text = (myreader["CheckMethod3"].ToString());
                        comboBox18.Text = (myreader["CheckMethod4"].ToString());
                        comboBox19.Text = (myreader["CheckMethod5"].ToString());
                        comboBox20.Text = (myreader["CheckMethod6"].ToString());
                        comboBox21.Text = (myreader["CheckMethod7"].ToString());
                        comboBox22.Text = (myreader["CheckMethod8"].ToString());
                        comboBox23.Text = (myreader["CheckMethod9"].ToString());
                        comboBox24.Text = (myreader["CheckMethod10"].ToString());


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                myreader.Close();
                
                SqlDataReader myreader2 = da.DataReader(defectdesc);

                while (myreader2.Read())
                {
                    try
                    {
                        comboBox25.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox26.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox27.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox28.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox29.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox30.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox31.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox32.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox33.Items.Add(myreader2.GetValue(0).ToString());
                        comboBox34.Items.Add(myreader2.GetValue(0).ToString());

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                }
                myreader2.Close();
                
                SqlDataReader myreader3 = da.DataReader(desc);

                while (myreader3.Read())
                {
                    try
                    {
                        comboBox5.Items.Add(myreader3["Description"].ToString());
                        comboBox6.Items.Add(myreader3["Description"].ToString());
                        comboBox7.Items.Add(myreader3["Description"].ToString());
                        comboBox8.Items.Add(myreader3["Description"].ToString());
                        comboBox9.Items.Add(myreader3["Description"].ToString());
                        comboBox10.Items.Add(myreader3["Description"].ToString());
                        comboBox11.Items.Add(myreader3["Description"].ToString());
                        comboBox12.Items.Add(myreader3["Description"].ToString());
                        comboBox13.Items.Add(myreader3["Description"].ToString());
                        comboBox14.Items.Add(myreader3["Description"].ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                myreader3.Close();
                
                SqlDataReader myreader4 = da.DataReader(checkmethod);

                while (myreader4.Read())
                {
                    try
                    {
                        comboBox15.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox16.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox17.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox18.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox19.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox20.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox21.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox22.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox23.Items.Add(myreader4["CheckMethod"].ToString());
                        comboBox24.Items.Add(myreader4["CheckMethod"].ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                }
                myreader4.Close();
                da.CloseConnection();

            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
            
            //End SQL Connection ------------------------------------------------
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.ShowDialog();

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            //textBox4.Clear(); AUTO INPUT SPACE FOR REV
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
            textBox13.Clear();
            textBox14.Clear();
            textBox15.Clear();
            textBox16.Clear();
            textBox17.Clear();
            textBox18.Clear();
            textBox19.Clear();
            textBox20.Clear();
            textBox21.Clear();
            textBox22.Clear();
            textBox23.Clear();
            textBox24.Clear();
            textBox25.Clear();
            textBox26.Clear();
            textBox27.Clear();
            textBox28.Clear();
            textBox29.Clear();
            textBox30.Clear();
            textBox31.Clear();
            textBox32.Clear();
            textBoxx1.Clear();
            textBoxx2.Clear();
            textBoxx3.Clear();
            comboBox1.Text = "";
            comboBox2.Text = "";
            comboBox3.Text = "";
            comboBox4.Text = "";
            comboBox5.Text = "";
            comboBox6.Text = "";
            comboBox7.Text = "";
            comboBox8.Text = "";
            comboBox9.Text = "";
            comboBox10.Text = "";
            comboBox11.Text = "";
            comboBox12.Text = "";
            comboBox13.Text = "";
            comboBox14.Text = "";
            comboBox15.Text = "";
            comboBox16.Text = "";
            comboBox17.Text = "";
            comboBox18.Text = "";
            comboBox19.Text = "";
            comboBox20.Text = "";
            comboBox21.Text = "";
            comboBox22.Text = "";
            comboBox23.Text = "";
            comboBox24.Text = "";
            comboBox25.Text = "";
            comboBox26.Text = "";
            comboBox27.Text = "";
            comboBox28.Text = "";
            comboBox29.Text = "";
            comboBox30.Text = "";
            comboBox31.Text = "";
            comboBox32.Text = "";
            comboBox33.Text = "";
            comboBox34.Text = "";
            textBox33.Text = "";
            textBox34.Text = "";
            textBox35.Text = "";
            textBox36.Text = "";
            textBox37.Text = "";
        }

        private void Button5_Click(object sender, EventArgs e)
        {
          
            CaptureScreen();
            printDialog1.ShowDialog();
            printDocument1.Print();
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);

        }

        Bitmap memoryImage;

        private void CaptureScreen()
        {
            printDocument1.DefaultPageSettings.Landscape = true;
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }


        private void Button6_Click(object sender, EventArgs e)
        {
            //Form3 frm3 = new Form3();
            //frm3.ShowDialog();

            MessageBox.Show("This feature is still under simulation..");

        }

        private void Button7_Click(object sender, EventArgs e)
        {

            if (IsInGroup("GEI Product Audit Admins"))
            {                
                groupBox3.Show();
                groupBox4.Show();
            }
            else
            {
                groupBox3.Hide();
                groupBox4.Hide();
            }
            try
            {
                //Sql connection to populate Manual Entry ComboBoxes ------------------------------------

                DataAccess da = new DataAccess();
                da.OpenConnection();              
                
                //Select CheckMethod Qry
                string checkmethod = "Select CheckMethod from tblCheckMethods";

                //Select Description Qry
                string desc = "Select Description from tblDescriptions WHERE Description is NOT NULL";

                //Select DefectDescription Qry
                string defectdesc = "Select DefectDescription from tblDefectDescriptions";
                

                SqlDataReader myreader = da.DataReader(checkmethod);

                while (myreader.Read())
                {
                    comboBox15.Items.Add(myreader.GetValue(0).ToString());
                    comboBox16.Items.Add(myreader.GetValue(0).ToString());
                    comboBox17.Items.Add(myreader.GetValue(0).ToString());   //check methods 15-24
                    comboBox18.Items.Add(myreader.GetValue(0).ToString());
                    comboBox19.Items.Add(myreader.GetValue(0).ToString());
                    comboBox20.Items.Add(myreader.GetValue(0).ToString());
                    comboBox21.Items.Add(myreader.GetValue(0).ToString());
                    comboBox22.Items.Add(myreader.GetValue(0).ToString());
                    comboBox23.Items.Add(myreader.GetValue(0).ToString());
                    comboBox24.Items.Add(myreader.GetValue(0).ToString());

                }
                myreader.Close();               

                SqlDataReader myreader2 = da.DataReader(desc);

                while (myreader2.Read())
                {
                    comboBox5.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox6.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox7.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox8.Items.Add(myreader2.GetValue(0).ToString());  //description 5-14
                    comboBox9.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox10.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox11.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox12.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox13.Items.Add(myreader2.GetValue(0).ToString());
                    comboBox14.Items.Add(myreader2.GetValue(0).ToString());

                }

                myreader2.Close();
                

                SqlDataReader myreader3 = da.DataReader(defectdesc);

                while (myreader3.Read())
                {
                    comboBox25.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox26.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox27.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox28.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox29.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox30.Items.Add(myreader3.GetValue(0).ToString());     //defect description 25-34
                    comboBox31.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox32.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox33.Items.Add(myreader3.GetValue(0).ToString());
                    comboBox34.Items.Add(myreader3.GetValue(0).ToString());


                }
                myreader3.Close();               
                
                da.CloseConnection();
                //End Sql Connection to populate Manual Entry

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void Button8_Click(object sender, EventArgs e)
        {
            if (IsInGroup("GEI Product Audit Admins"))
            {
                groupBox2.Show();
                groupBox3.Show();
                groupBox4.Show();
            }
            else
            {
                groupBox2.Hide();
                groupBox3.Hide();
                groupBox4.Hide();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox2.Hide();
            groupBox3.Hide();
            groupBox4.Hide();
            textBox36.Text = Environment.UserName.ToString();
        }        

        private void Button9_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4();
            frm4.ShowDialog();

        }

        private void Button10_Click(object sender, EventArgs e)
        {
            Form5 frm5 = new Form5(textBox38.Text, textBox2.Text, textBox39.Text, textBox3.Text, textBox6.Text);
            frm5.ShowDialog();

        }
    }    
}
