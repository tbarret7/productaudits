﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ProductAudit
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

       

        private void Form4_Load(object sender, EventArgs e)
        {
            

        }
        private static DataTable GetData(string sqlCommand, string mytitle)
        {
            string wo = mytitle;
            string connectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_PRODUCT_AUDITS; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
                    
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@wo", wo);

            adapter.Fill(table);           

            return table;
            
        }       

        private void Button1_Click(object sender, EventArgs e)
        {
            string wo = textBox1.Text;

            try
            {
                tblQualityAuditsBindingSource.DataSource = GetData("SELECT rec_id, AuditType, PartNumber, WONum, Line, Oper, Rev, InspectionLevel, " +
                    "Customer, ShopOrder, SampleSize, CustomerPartNumber, CustomerRev, OperationSeq, DieNumber, Lot_Size, AuditorDefectComments, " +
                    "Disposition, ClosedDate, DMRNum, OperNo, AuditorName, ShiftNum, Description, Description2, Description3, Description4, " +
                    "Description5, Description6, Description7, Description8, Description9, Description10, CheckMethod1, CheckMethod2, " +
                    "CheckMethod3, CheckMethod4, CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10, " +
                    "DefectDescription1, DefectDescription2, DefectDescription3, DefectDescription4, DefectDescription5, DefectDescription6, " +
                    "DefectDescription7, DefectDescription8, DefectDescription9, DefectDescription10 " +
                    "FROM dbo.tblQualityAudits WHERE WONum = @wo", wo);
                tblQualityAuditsDataGridView.DataSource = tblQualityAuditsBindingSource;
            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
        }

    }
}
