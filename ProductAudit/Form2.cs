﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.IO;

namespace ProductAudit
{
    public partial class Form2 : Form
    {
        string path;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //Load Sql connection to populate comboboxes

            DataAccess da = new DataAccess();
            da.OpenConnection();           

            //CheckMethod Qry
            string checkmethod = "Select CheckMethod from tblCheckMethods";

            //Description Qry
            string desc = "Select Description from tblDescriptions WHERE Description is NOT NULL";

            SqlDataReader myreader = da.DataReader(checkmethod);

            while (myreader.Read())
            {
                comboBox11.Items.Add(myreader.GetValue(0).ToString());
                comboBox12.Items.Add(myreader.GetValue(0).ToString());
                comboBox13.Items.Add(myreader.GetValue(0).ToString());
                comboBox14.Items.Add(myreader.GetValue(0).ToString());
                comboBox15.Items.Add(myreader.GetValue(0).ToString());
                comboBox16.Items.Add(myreader.GetValue(0).ToString());
                comboBox26.Items.Add(myreader.GetValue(0).ToString());
                comboBox27.Items.Add(myreader.GetValue(0).ToString());
                comboBox28.Items.Add(myreader.GetValue(0).ToString());
                comboBox29.Items.Add(myreader.GetValue(0).ToString());

            }
            myreader.Close();

            SqlDataReader myreader2 = da.DataReader(desc);

            while (myreader2.Read())
            {
                comboBox5.Items.Add(myreader2.GetValue(0).ToString());
                comboBox6.Items.Add(myreader2.GetValue(0).ToString());
                comboBox7.Items.Add(myreader2.GetValue(0).ToString());
                comboBox8.Items.Add(myreader2.GetValue(0).ToString());
                comboBox9.Items.Add(myreader2.GetValue(0).ToString());
                comboBox10.Items.Add(myreader2.GetValue(0).ToString());
                comboBox1.Items.Add(myreader2.GetValue(0).ToString());
                comboBox23.Items.Add(myreader2.GetValue(0).ToString());
                comboBox24.Items.Add(myreader2.GetValue(0).ToString());
                comboBox25.Items.Add(myreader2.GetValue(0).ToString());

            }

            myreader2.Close();

            da.CloseConnection();
            
            //END BOX POPULATION

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
            string d1 = "";
            string d2 = "";
            string d3 = "";
            string d4 = "";
            string d5 = "";
            string d6 = "";
            string d7 = "";
            string d8 = "";
            string d9 = "";
            string d10 = "";
            string c1 = "";
            string c2 = "";
            string c3 = "";
            string c4 = "";
            string c5 = "";
            string c6 = "";
            string c7 = "";
            string c8 = "";
            string c9 = "";
            string c10 = "";
            string partNum = "";           
            string rev = "";
            string die = "";

            partNum = textBox1.Text;
            rev = textBox4.Text;
            die = textBox5.Text;

            d1 = comboBox5.Text.ToString();
            d2 = comboBox6.Text.ToString();
            d3 = comboBox7.Text.ToString();
            d4 = comboBox8.Text.ToString();
            d5 = comboBox9.Text.ToString();
            d6 = comboBox10.Text.ToString();
            d7 = comboBox1.Text.ToString();
            d8 = comboBox23.Text.ToString();
            d9 = comboBox24.Text.ToString();
            d10 = comboBox25.Text.ToString();

            List<string> descArray = new List<string>();

            descArray.Add(d1);
            descArray.Add(d2);
            descArray.Add(d3);
            descArray.Add(d4);
            descArray.Add(d5);
            descArray.Add(d6);
            descArray.Add(d7);
            descArray.Add(d8);
            descArray.Add(d9);
            descArray.Add(d10);

            int sizeofList = descArray.Count;

            c1 = comboBox11.Text.ToString();
            c2 = comboBox12.Text.ToString();
            c3 = comboBox13.Text.ToString();
            c4 = comboBox14.Text.ToString();
            c5 = comboBox15.Text.ToString();
            c6 = comboBox16.Text.ToString();
            c7 = comboBox26.Text.ToString();
            c8 = comboBox27.Text.ToString();
            c9 = comboBox28.Text.ToString();
            c10 = comboBox29.Text.ToString();

            List<string> checkArray = new List<string>();
            checkArray.Add(c1);
            checkArray.Add(c2);
            checkArray.Add(c3);
            checkArray.Add(c4);
            checkArray.Add(c5);
            checkArray.Add(c6);
            checkArray.Add(c7);
            checkArray.Add(c8);
            checkArray.Add(c9);
            checkArray.Add(c10);

            int sizeOfList = checkArray.Count;

            //Init SQL Connection-----------------------------------------------------------------

            DataAccess da = new DataAccess();
            da.OpenConnection();

            //Image parse
            byte[] data = System.IO.File.ReadAllBytes(path);

            //Insert new recipe Qry
            string connectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_PRODUCT_AUDITS; Integrated Security = SSPI"; 
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("Insert into tblAuditRecipe (PartNumber, Rev, Die, ImageAttachment, Description, Description2, Description3, " +
                "Description4, Description5, Description6, Description7, Description8, Description9, Description10, CheckMethod, CheckMethod2, " +
                "CheckMethod3, CheckMethod4, CheckMethod5, CheckMethod6,CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10) Select '"
                + partNum + "' as PartNumber_Ins, '" + rev + "' as Rev_Ins, '" + die + "' as Die_INS, @img as ImageAttachment_INS, '" + d1 + "' as Description_Ins, '" + d2 + "' as Description2_Ins, '"
                + d3 + "' as Description3_Ins, '" + d4 + "' as Description4_Ins, '" + d5 + "' as Description5_Ins, '" + d6 + "' as Description6_Ins, '"
                + d7 + "' as Description7_Ins, '" + d8 + "' as Description8_Ins, '" + d9 + "' as Description9_Ins, '" + d10 + "' as Description10_Ins, '"
                + c1 + "' as CheckMethod_Ins, '" + c2 + "' as CheckMethod2_Ins, '" + c3 + "' as CheckMethod3_Ins, '" + c4 + "' as CheckMethod4_Ins, '"
                + c5 + "' as CheckMethod5_Ins, '" + c6 + "' as CheckMethod6_Ins, '" + c7 + "' as CheckMethod7_Ins, '" + c8 + "' as CheckMethod8_Ins, '"
                + c9 + "' as CheckMethod9_Ins, '" + c10 + "' as CheckMethod10_Ins", conn);
            command.Parameters.AddWithValue("@img", data);
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
            command.Dispose();

                     

            for (int i = 0; i < sizeOfList; i++) 
            {
                try
                {
                    var checks = checkArray[i];
                    if (checks != "")
                    {
                        string cmd = "Insert into tblCheckMethods (CheckMethod) Select '" + checks + "' as CheckMethod_Ins";
                        da.ExecuteQueries(cmd);
                        
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            for (int i = 0; i < sizeofList; i++)
            {
                try
                {
                    var descs = descArray[i];
                    if (descs != "")
                    {
                        string cmd = "Insert into tblDescriptions (Description) Select '" + descs + "' as Description_Ins";
                        da.ExecuteQueries(cmd);                        

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            MessageBox.Show("You have successfully updated this recipe.");
            
        }
               
        private void Button5_Click(object sender, EventArgs e)
        {
            string geiItem = textBox2.Text;
            string geiRev = textBox3.Text;
            textBox1.Text = geiItem;
            textBox4.Text = geiRev;
            button1.Hide();
            //Init SQL Connection-----------------------------------------------------------------
            DataAccess da = new DataAccess();

            da.OpenConnection();

            //Select Fields Qry
            string recipe = "Select PartNumber, Rev, Description, Description2, Description3, Description4, Description5, Description6, "
                + "Description7, Description8, Description9, Description10, CheckMethod, CheckMethod2, CheckMethod3, CheckMethod4, "
                + "CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10 FROM tblAuditRecipe WHERE PartNumber = '"
                + geiItem + "'";
           
            SqlDataReader myreader = da.DataReader(recipe);

            while (myreader.Read())
            {
                try
                {
                    
                    comboBox5.Text = (myreader["Description"].ToString());
                    comboBox6.Text = (myreader["Description2"].ToString());
                    comboBox7.Text = (myreader["Description3"].ToString());
                    comboBox8.Text = (myreader["Description4"].ToString());
                    comboBox9.Text = (myreader["Description5"].ToString());
                    comboBox10.Text = (myreader["Description6"].ToString());
                    comboBox1.Text = (myreader["Description7"].ToString());
                    comboBox23.Text = (myreader["Description8"].ToString());
                    comboBox24.Text = (myreader["Description9"].ToString());
                    comboBox25.Text = (myreader["Description10"].ToString());
                    comboBox11.Text = (myreader["CheckMethod"].ToString());
                    comboBox12.Text = (myreader["CheckMethod2"].ToString());
                    comboBox13.Text = (myreader["CheckMethod3"].ToString());
                    comboBox14.Text = (myreader["CheckMethod4"].ToString());
                    comboBox15.Text = (myreader["CheckMethod5"].ToString());
                    comboBox16.Text = (myreader["CheckMethod6"].ToString());
                    comboBox26.Text = (myreader["CheckMethod6"].ToString());
                    comboBox27.Text = (myreader["CheckMethod6"].ToString());
                    comboBox28.Text = (myreader["CheckMethod6"].ToString());
                    comboBox29.Text = (myreader["CheckMethod6"].ToString());
                    

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
            da.CloseConnection();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path = dialog.FileName;
                pictureBox1.Image = Image.FromFile(path);
                

            }

        }

        private void Button3_Click(object sender, EventArgs e)
        {

            string partNum = textBox1.Text;
            string rev = textBox4.Text;
            string die = textBox5.Text;

            string d1 = comboBox5.Text.ToString();
            string d2 = comboBox6.Text.ToString();
            string d3 = comboBox7.Text.ToString();
            string d4 = comboBox8.Text.ToString();
            string d5 = comboBox9.Text.ToString();
            string d6 = comboBox10.Text.ToString();
            string d7 = comboBox1.Text.ToString();
            string d8 = comboBox23.Text.ToString();
            string d9 = comboBox24.Text.ToString();
            string d10 = comboBox25.Text.ToString();
            string c1 = comboBox11.Text.ToString();
            string c2 = comboBox12.Text.ToString();
            string c3 = comboBox13.Text.ToString();
            string c4 = comboBox14.Text.ToString();
            string c5 = comboBox15.Text.ToString();
            string c6 = comboBox16.Text.ToString();
            string c7 = comboBox26.Text.ToString();
            string c8 = comboBox27.Text.ToString();
            string c9 = comboBox28.Text.ToString();
            string c10 = comboBox29.Text.ToString();

            //update recipe
            byte[] data = System.IO.File.ReadAllBytes(path);

            string connectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_PRODUCT_AUDITS; Integrated Security = SSPI";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("UPDATE tblAuditRecipe SET PartNumber = @pn, Rev = @rev, Description = @desc, Die = @die, " +
                "ImageAttachment = @img, Description2 = @desc2, Description3 = @desc3, Description4 = @desc4, Description5 = @desc5, " +
                "Description6 = @desc6, Description7 = @desc7, Description8 = @desc8, Description9 = @desc9, Description10 = @desc10, " +
                "CheckMethod = @cm, CheckMethod2 = @cm2, CheckMethod3 = @cm3, CheckMethod4 = @cm4, CheckMethod5 = @cm5, CheckMethod6 = @cm6, " +
                "CheckMethod7 = @cm7, CheckMethod8 = @cm8, CheckMethod9 = @cm9, CheckMethod10 = @cm10 WHERE PartNumber = @pn", conn);
            command.Parameters.AddWithValue("@pn", partNum);
            command.Parameters.AddWithValue("@rev", rev);
            command.Parameters.AddWithValue("@desc", d1);
            command.Parameters.AddWithValue("@die", die);
            command.Parameters.AddWithValue("@img", data);
            command.Parameters.AddWithValue("@desc2", d2);
            command.Parameters.AddWithValue("@desc3", d3);
            command.Parameters.AddWithValue("@desc4", d4);
            command.Parameters.AddWithValue("@desc5", d5);
            command.Parameters.AddWithValue("@desc6", d6);
            command.Parameters.AddWithValue("@desc7", d7);
            command.Parameters.AddWithValue("@desc8", d8);
            command.Parameters.AddWithValue("@desc9", d9);
            command.Parameters.AddWithValue("@desc10", d10);
            command.Parameters.AddWithValue("@cm", c1);
            command.Parameters.AddWithValue("@cm2", c2);
            command.Parameters.AddWithValue("@cm3", c3);
            command.Parameters.AddWithValue("@cm4", c4);
            command.Parameters.AddWithValue("@cm5", c5);
            command.Parameters.AddWithValue("@cm6", c6);
            command.Parameters.AddWithValue("@cm7", c7);
            command.Parameters.AddWithValue("@cm8", c8);
            command.Parameters.AddWithValue("@cm9", c9);
            command.Parameters.AddWithValue("@cm10", c10);
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
            command.Dispose();
            MessageBox.Show("You have succesfully updated this record...");

            this.Close();
            

            //"Select PartNumber, Rev, Description, Description2, Description3, Description4, Description5, Description6, "
            // +"Description7, Description8, Description9, Description10, CheckMethod, CheckMethod2, CheckMethod3, CheckMethod4, "
            // + "CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10 FROM tblAuditRecipe WHERE PartNumber = '"
            // + geiItem + "'";
        }
    }
}
